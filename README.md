# Programming Python Beyond The Basics

The Jupyter notebook in this repo has been used in a programming course that was given in 2022 at Astron.

The notebook is licensed under the CC-BY license.
